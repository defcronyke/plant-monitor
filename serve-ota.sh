#!/bin/sh
#
# Run the OTA Update server. If you don't have https
# certificates or dhparam yet, it will generate them.
#

if [ ! -d "server_certs" ]; then
    mkdir server_certs
fi

if [ ! -f "server_certs/ca_cert.pem" ] || [ ! -f "server_certs/ca_key.pem" ]; then
    ./gen-cert.sh

    if [ -d "build" ]; then
        rm -f build/ca_cert.pem build/ca_key.pem
    fi
fi

if [ ! -f "server_certs/dhparam.pem" ]; then
    ./gen-dhparam.sh

    if [ -d "build" ]; then
        rm -f build/dhparam.pem
    fi
fi

if [ ! -d "build" ]; then
    mkdir build
fi

cd build

if [ ! -f "ca_cert.pem" ] || [ ! -f "ca_key.pem" ] || [ ! -f "dhparam.pem" ]; then
    cp ../server_certs/*.pem .
fi

openssl s_server -WWW -key ca_key.pem -cert ca_cert.pem -dhparam dhparam.pem -port 8096

cd ..
