/* ESP HTTP Client Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"
#include "esp_tls.h"
#include "cJSON.h"

#include "esp_http_client.h"
#include "plant_moist.h"

#define MAX_HTTP_RECV_BUFFER    512

#define NUM_MOIST_SENSORS       6

static const char *TAG = "plant-http-client";

/* Root cert for howsmyssl.com, taken from howsmyssl_com_root_cert.pem

   The PEM file was extracted from the output of this command:
   openssl s_client -showcerts -connect www.howsmyssl.com:443 </dev/null

   The CA root cert is the last cert given in the chain of certs.

   To embed it in the app binary, the PEM file is named
   in the component.mk COMPONENT_EMBED_TXTFILES variable.
*/
extern const char howsmyssl_com_root_cert_pem_start[] asm("_binary_howsmyssl_com_root_cert_pem_start");
extern const char howsmyssl_com_root_cert_pem_end[]   asm("_binary_howsmyssl_com_root_cert_pem_end");

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            if (!esp_http_client_is_chunked_response(evt->client)) {
                // Write out data
                // printf("%.*s", evt->data_len, (char*)evt->data);
            }

            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
            int mbedtls_err = 0;
            esp_err_t err = esp_tls_get_and_clear_last_error(evt->data, &mbedtls_err, NULL);
            if (err != 0) {
                ESP_LOGI(TAG, "Last esp error code: 0x%x", err);
                ESP_LOGI(TAG, "Last mbedtls failure: 0x%x", mbedtls_err);
            }
            break;
    }
    return ESP_OK;
}

bool water_low[NUM_MOIST_SENSORS] = {
    false,
    // false,
    // false,
    false,
    false,
    false,
    false,
    false,
};

char* get_email_to() {
    cJSON *to_arr = cJSON_CreateArray();

    char email_to_buf[strlen(CONFIG_EMAIL_TO"\0") + 1];

    memcpy(email_to_buf, CONFIG_EMAIL_TO, strlen(CONFIG_EMAIL_TO"\0"));

    email_to_buf[strlen(CONFIG_EMAIL_TO"\0")] = '\0';

    char* delim = ",";
    char* ptr = strtok(email_to_buf, delim);

    while (ptr != NULL) {
        cJSON* to_obj = cJSON_CreateObject();
        cJSON_AddStringToObject(to_obj, "email", ptr);
        cJSON_AddItemToArray(to_arr, to_obj);

        ptr = strtok(NULL, delim);
    }

    char* to_str = cJSON_Print(to_arr);

    cJSON_Delete(to_arr);

    return to_str;
}

static void https_with_url()
{
    double watered[NUM_MOIST_SENSORS];

    const char* monitorUrl = CONFIG_MONITOR_URL;
    const char* monitorUrl2 = CONFIG_MONITOR_URL2;

    const uint32_t mutexWaitTicks = 100000;
	if (xSemaphoreTake(xMutex, (TickType_t)mutexWaitTicks) == pdTRUE)
	{
        watered[0] = moist_percent_shared0;
        watered[1] = moist_percent_shared3;
        watered[2] = moist_percent_shared4;
        watered[3] = moist_percent_shared5;
        watered[4] = moist_percent_shared6;
        watered[5] = moist_percent_shared7;
        // watered[6] = moist_percent_shared6;
        // watered[7] = moist_percent_shared7;

        // watered[0] = ((float)moist_adc_reading_shared0 / (float)moist_resolution_shared) * 100.f;
        // watered[1] = ((float)moist_adc_reading_shared1 / (float)moist_resolution_shared) * 100.f;
        // watered[2] = ((float)moist_adc_reading_shared2 / (float)moist_resolution_shared) * 100.f;
        // watered[3] = ((float)moist_adc_reading_shared3 / (float)moist_resolution_shared) * 100.f;
        // watered[4] = ((float)moist_adc_reading_shared4 / (float)moist_resolution_shared) * 100.f;
        // watered[5] = ((float)moist_adc_reading_shared5 / (float)moist_resolution_shared) * 100.f;
        // watered[6] = ((float)moist_adc_reading_shared6 / (float)moist_resolution_shared) * 100.f;
        // watered[7] = ((float)moist_adc_reading_shared7 / (float)moist_resolution_shared) * 100.f;

        xSemaphoreGive(xMutex);

        for (uint i = 0; i < NUM_MOIST_SENSORS; i++) {
            // If we need to send email.
            if (!water_low[i] && watered[i] < CONFIG_EMAIL_WATERED_THRESHOLD) {
                water_low[i] = true;

                char* to_str = get_email_to();

                const char* auth_header = "Bearer "CONFIG_EMAIL_SENDGRID_API_KEY"\0";
                const char* content_type_header = "application/json\0";
                const char* bodyStr = "{\
                    \"personalizations\": [{\"to\": %s}],\
                        \"from\": {\"email\": \""CONFIG_EMAIL_FROM"\"},\
                        \"subject\": \"Plant #%d: Water me please. I\'m thirsty! (water level: %f%%)\",\
                        \"content\": [{\
                        \"type\": \"text/plain\", \
                            \"value\": \"Plant #%d: My current water level is: %f%%\\n"
                                "\\n"
                                "Check here for current sensor readings:\\n"
                                "\\n"
                                "%s\\n"
                                "\\n"
                                "%s\\n\""\
                            "}]\
                }\0";

                char body[strlen(bodyStr) + strlen(to_str) + 100];

                sprintf(body, bodyStr, to_str, i + 1, watered[i], i + 1, watered[i], monitorUrl, monitorUrl2);

                ESP_LOGI(TAG, "!! email body: %s", body);

                free((void *) to_str);

                esp_http_client_config_t config = {
                    .url = CONFIG_EMAIL_URL,
                    .event_handler = _http_event_handler,
                };
                esp_http_client_handle_t client = esp_http_client_init(&config);

                esp_http_client_set_method(client, HTTP_METHOD_POST);

                esp_http_client_set_header(client, "Authorization", auth_header);
                esp_http_client_set_header(client, "Content-Type", content_type_header);

                esp_http_client_set_post_field(client, body, strlen(body));

                esp_err_t err;

                ESP_LOGI(TAG, "Water low on plant %d. Sending email notifications.", i + 1);

                while (1) {
                    err = esp_http_client_perform(client);
                    if (err != ESP_ERR_HTTP_EAGAIN) {
                        break;
                    }
                }

                if (err == ESP_OK) {
                    ESP_LOGI(TAG, "HTTPS Status = %d, content_length = %d",
                            esp_http_client_get_status_code(client),
                            esp_http_client_get_content_length(client));
                } else {
                    ESP_LOGE(TAG, "Error perform http request %s", esp_err_to_name(err));
                }

                esp_http_client_cleanup(client);

            // If water low but email already sent.
            } else if (water_low[i] && watered[i] < CONFIG_EMAIL_WATERED_THRESHOLD) {
                ESP_LOGI(TAG, "Water low, already notified by email: %d : %f%%", i + 1, watered[i]);
            
            // If water was low but now it's high again.
            } else if (water_low[i] && watered[i] >= CONFIG_EMAIL_WATERED_THRESHOLD2) {
                water_low[i] = false;

                ESP_LOGI(TAG, "Water not low anymore: %d : %f%%", i + 1, watered[i]);
            
                char* to_str = get_email_to();

                const char* auth_header = "Bearer "CONFIG_EMAIL_SENDGRID_API_KEY"\0";
                const char* content_type_header = "application/json\0";
                // const char* bodyStr = "{\"personalizations\": [{\"to\": %s}],\"from\": {\"email\": \""CONFIG_EMAIL_FROM"\"},\"subject\": \"Plant #%d: I\'m not thirsty anymore. Thank you for watering me!\",\"content\": [{\"type\": \"text/plain\", \"value\": \"Plant #%d: My current water level is: %f%%\"}]}\0";
                
                const char* bodyStr = "{\
                    \"personalizations\": [{\"to\": %s}],\
                        \"from\": {\"email\": \""CONFIG_EMAIL_FROM"\"},\
                        \"subject\": \"Plant #%d: I\'m not thirsty anymore. Thank you for watering me! (water level: %f%%)\",\
                        \"content\": [{\"type\": \"text/plain\", \
                        \"value\": \"Plant #%d: My current water level is: %f%%\\n"
                                "\\n"
                                "Check here for current sensor readings:\\n"
                                "\\n"
                                "%s\\n"
                                "\\n"
                                "%s\\n\""
                            "}]\
                }\0";
                
                char body[strlen(bodyStr) + strlen(to_str) + 100];

                sprintf(body, bodyStr, to_str, i + 1, watered[i], i + 1, watered[i], monitorUrl, monitorUrl2);
                // sprintf(body, bodyStr, to_str, i + 1, i + 1, watered[i]);

                ESP_LOGI(TAG, "!! email body: %s", body);

                free((void *) to_str);

                esp_http_client_config_t config = {
                    .url = CONFIG_EMAIL_URL,
                    .event_handler = _http_event_handler,
                };
                esp_http_client_handle_t client = esp_http_client_init(&config);

                esp_http_client_set_method(client, HTTP_METHOD_POST);

                esp_http_client_set_header(client, "Authorization", auth_header);
                esp_http_client_set_header(client, "Content-Type", content_type_header);

                esp_http_client_set_post_field(client, body, strlen(body));

                esp_err_t err;

                while (1) {
                    err = esp_http_client_perform(client);
                    if (err != ESP_ERR_HTTP_EAGAIN) {
                        break;
                    }
                }

                if (err == ESP_OK) {
                    ESP_LOGI(TAG, "HTTPS Status = %d, content_length = %d",
                            esp_http_client_get_status_code(client),
                            esp_http_client_get_content_length(client));
                } else {
                    ESP_LOGE(TAG, "Error perform http request %s", esp_err_to_name(err));
                }

                esp_http_client_cleanup(client);
            }
        }
	}
	else
	{
		ESP_LOGW(TAG, "Couldn't take mutex within %d ticks.", mutexWaitTicks);
    }
}

static void email_notify_task(void *pvParameters)
{
    ESP_LOGI(TAG, "Start email notify task");

    for (;;) {
        https_with_url();

        vTaskDelay((CONFIG_EMAIL_WATERED_TIMEOUT * 1000) / portTICK_PERIOD_MS);
    }

    ESP_LOGI(TAG, "Finish email notify task");

    vTaskDelete(NULL);
}

void plant_http_client_main()
{
    xTaskCreate(&email_notify_task, "email_notify_task", 8192, NULL, 5, NULL);
}
