#include "plant_moist.h"
#include <freertos/task.h>
#include "esp_system.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include <time.h>
#include <errno.h>

#define DEFAULT_VREF 1100 // Use adc2_vref_to_gpio() to obtain a better estimate

#define NUM_SAMPLES	CONFIG_NUM_SAMPLES	// Multisampling

// Measured from a previous dry (air) sensor reading for calibration purposes.
#define DRY_SENSOR_VALUE_RAW1    	CONFIG_DRY_SENSOR_VALUE_RAW1    

// Measured from a previous wet (cup of water) sensor reading for calibration purposes.
#define WET_SENSOR_VALUE_RAW1    	CONFIG_WET_SENSOR_VALUE_RAW1    

// The sensor isn't connected or isn't working if the reading is lower than this value. In that case give a 0.0% reading.
#define DRY_IF_BELOW_RAW1			CONFIG_DRY_IF_BELOW_RAW1

#define DRY_SENSOR_VALUE_RAW2    	CONFIG_DRY_SENSOR_VALUE_RAW2
#define WET_SENSOR_VALUE_RAW2		CONFIG_WET_SENSOR_VALUE_RAW2
#define DRY_IF_BELOW_RAW2			CONFIG_DRY_IF_BELOW_RAW2

#define DRY_SENSOR_VALUE_RAW3    	CONFIG_DRY_SENSOR_VALUE_RAW3
#define WET_SENSOR_VALUE_RAW3    	CONFIG_WET_SENSOR_VALUE_RAW3
#define DRY_IF_BELOW_RAW3			CONFIG_DRY_IF_BELOW_RAW3

#define DRY_SENSOR_VALUE_RAW4    	CONFIG_DRY_SENSOR_VALUE_RAW4
#define WET_SENSOR_VALUE_RAW4    	CONFIG_WET_SENSOR_VALUE_RAW4
#define DRY_IF_BELOW_RAW4			CONFIG_DRY_IF_BELOW_RAW4

#define DRY_SENSOR_VALUE_RAW5    	CONFIG_DRY_SENSOR_VALUE_RAW5
#define WET_SENSOR_VALUE_RAW5    	CONFIG_WET_SENSOR_VALUE_RAW5
#define DRY_IF_BELOW_RAW5			CONFIG_DRY_IF_BELOW_RAW5

#define DRY_SENSOR_VALUE_RAW6    	CONFIG_DRY_SENSOR_VALUE_RAW6
#define WET_SENSOR_VALUE_RAW6    	CONFIG_WET_SENSOR_VALUE_RAW6
#define DRY_IF_BELOW_RAW6			CONFIG_DRY_IF_BELOW_RAW6

static esp_adc_cal_characteristics_t *adc_chars;

static const adc_channel_t channel0 = ADC_CHANNEL_0;	// SVP
// static const adc_channel_t channel1 = ADC_CHANNEL_1;	// Not available on DevkitC
// static const adc_channel_t channel2 = ADC_CHANNEL_2;	// Not available on DevkitC
static const adc_channel_t channel3 = ADC_CHANNEL_3;	// SVN
static const adc_channel_t channel4 = ADC_CHANNEL_4;	// GPIO32
static const adc_channel_t channel5 = ADC_CHANNEL_5;	// GPIO33
static const adc_channel_t channel6 = ADC_CHANNEL_6;	// GPIO34
static const adc_channel_t channel7 = ADC_CHANNEL_7;	// GPIO35

static const adc_atten_t atten = ADC_ATTEN_DB_0;
static const adc_unit_t unit = ADC_UNIT_1;
// static const adc_unit_t unit = ADC_UNIT_2;

static const uint32_t resolution = 4095;

static const char *MOIST_TAG = "plant-moist-read";

SemaphoreHandle_t xMutex;

int hours_shared, minutes_shared, seconds_shared, day_shared, month_shared, year_shared;

uint32_t moist_resolution_shared;

double moist_percent_shared0;
// double moist_percent_shared1;
// double moist_percent_shared2;
double moist_percent_shared3;
double moist_percent_shared4;
double moist_percent_shared5;
double moist_percent_shared6;
double moist_percent_shared7;

uint32_t moist_adc_reading_shared0;
// uint32_t moist_adc_reading_shared1;
// uint32_t moist_adc_reading_shared2;
uint32_t moist_adc_reading_shared3;
uint32_t moist_adc_reading_shared4;
uint32_t moist_adc_reading_shared5;
uint32_t moist_adc_reading_shared6;
uint32_t moist_adc_reading_shared7;

uint32_t moist_voltage_shared0;
// uint32_t moist_voltage_shared1;
// uint32_t moist_voltage_shared2;
uint32_t moist_voltage_shared3;
uint32_t moist_voltage_shared4;
uint32_t moist_voltage_shared5;
uint32_t moist_voltage_shared6;
uint32_t moist_voltage_shared7;

/*  Map an 'input' value with a range of 'input_start' to 'input_end'
    into a different range of 'output_start' to 'output_end'.   */
double map(double input, double input_start, double input_end, double output_start, double output_end) {
    double output = output_start + (output_end - output_start) / (input_end - input_start) * (input - input_start);
    
    if (output < output_start) {
        return output_start;
    } else if (output > output_end) {
        return output_end;
    }

    return output;
}

static void check_efuse()
{
	//Check TP is burned into eFuse
	if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK)
	{
		printf("eFuse Two Point: Supported\n");
	}
	else
	{
		printf("eFuse Two Point: NOT supported\n");
	}

	//Check Vref is burned into eFuse
	if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_VREF) == ESP_OK)
	{
		printf("eFuse Vref: Supported\n");
	}
	else
	{
		printf("eFuse Vref: NOT supported\n");
	}
}

static void print_char_val_type(esp_adc_cal_value_t val_type)
{
	if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP)
	{
		printf("Characterized using Two Point Value\n\n");
	}
	else if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF)
	{
		printf("Characterized using eFuse Vref\n\n");
	}
	else
	{
		printf("Characterized using Default Vref\n\n");
	}
}

static esp_err_t read_moisture(void *arg)
{
	uint32_t adc_reading0 = 0;
	// uint32_t adc_reading1 = 0;
	// uint32_t adc_reading2 = 0;
	uint32_t adc_reading3 = 0;
	uint32_t adc_reading4 = 0;
	uint32_t adc_reading5 = 0;
	uint32_t adc_reading6 = 0;
	uint32_t adc_reading7 = 0;

	uint32_t adc_reading_print = 0;

	int hours, minutes, seconds, day, month, year;

	time_t now;

	time(&now);

	struct tm *local = localtime(&now);

	hours = local->tm_hour;	 // get hours since midnight (0-23)
	minutes = local->tm_min; // get minutes passed after the hour (0-59)
	seconds = local->tm_sec; // get seconds passed after minute (0-59)

	day = local->tm_mday;		  // get day of month (1 to 31)
	month = local->tm_mon + 1;	  // get month of year (0 to 11)
	year = local->tm_year + 1900; // get year since 1900

	// Multisampling
	ESP_LOGI(MOIST_TAG, "%d-%02d-%02d %02d:%02d:%02d | taking %d readings:\n", year, month, day, hours, minutes, seconds, NUM_SAMPLES);


	ESP_LOGI(MOIST_TAG, "sensor %d:\n", 0);
	for (int i = 0; i < NUM_SAMPLES; i++)
	{
		if (i != 0 && (i % 8) == 0)
		{
			printf("\n");
		}

		if (unit == ADC_UNIT_1)
		{
			adc_reading_print = adc1_get_raw((adc1_channel_t)channel0);
			adc_reading0 += adc_reading_print;
		}
		else
		{
			int raw;
			adc2_config_channel_atten((adc2_channel_t)channel0, atten);

			esp_err_t r = adc2_get_raw((adc2_channel_t)channel0, ADC_WIDTH_BIT_12, &raw);
			while (r != ESP_OK) {
				ESP_LOGW(MOIST_TAG, "Error taking reading from ADC2: %d - %s\n", r, esp_err_to_name(r));

				vTaskDelay(2 * portTICK_PERIOD_MS);
				
				r = adc2_get_raw((adc2_channel_t)channel0, ADC_WIDTH_BIT_12, &raw);
			}
		
			adc_reading_print = raw;
			adc_reading0 += adc_reading_print;
		}

		printf("%d\t", adc_reading_print);
	}
	printf("\n\n");

	adc_reading0 /= NUM_SAMPLES;

	// Convert adc_reading to voltage in mV
	uint32_t voltage0 = esp_adc_cal_raw_to_voltage(adc_reading0, adc_chars);

	double moist_percent0 = map((double)adc_reading0, (double)DRY_SENSOR_VALUE_RAW1, (double)WET_SENSOR_VALUE_RAW1, 0.0, 100.0);

	if (adc_reading0 < DRY_IF_BELOW_RAW1) {
		moist_percent0 = 0.0;
	}

	char *percent_spacing0 = "    \0";
	
	if (moist_percent0 >= 100.0) {
		percent_spacing0 = "  \0";
	} else if (moist_percent0 >= 10.0) {
		percent_spacing0 = "   \0";
	}

	ESP_LOGI(MOIST_TAG, "moisture percent: %.4f %%%sraw: %d(dry) | %d | %d(wet)\n", moist_percent0, percent_spacing0, DRY_SENSOR_VALUE_RAW1, adc_reading0, WET_SENSOR_VALUE_RAW1);

	// ESP_LOGI(MOIST_TAG, "watered: %.4f%%\traw: %d/%d\tvoltage: %dmV\n", ((double)adc_reading0 / (double)resolution) * 100, adc_reading0, resolution, voltage0);


	// ESP_LOGI(MOIST_TAG, "sensor %d:\n", 1);
	// for (int i = 0; i < NUM_SAMPLES; i++)
	// {
	// 	if (i != 0 && (i % 8) == 0)
	// 	{
	// 		printf("\n");
	// 	}

	// 	if (unit == ADC_UNIT_1)
	// 	{
	// 		adc_reading_print = adc1_get_raw((adc1_channel_t)channel1);
	// 		adc_reading1 += adc_reading_print;
	// 	}
	// 	else
	// 	{
	// 		int raw;
	// 		adc2_get_raw((adc2_channel_t)channel1, ADC_WIDTH_BIT_12, &raw);
	// 		adc_reading_print = raw;
	// 		adc_reading1 += adc_reading_print;
	// 	}

	// 	printf("%d\t", adc_reading_print);
	// }
	// printf("\n\n");

	// adc_reading1 /= NUM_SAMPLES;

	// // Convert adc_reading to voltage in mV
	// uint32_t voltage1 = esp_adc_cal_raw_to_voltage(adc_reading1, adc_chars);

	// double moist_percent1 = map((double)adc_reading1, (double)DRY_SENSOR_VALUE_RAW, (double)WET_SENSOR_VALUE_RAW, 0.0, 100.0);

	// char *percent_spacing1 = "    \0";
	
	// if (moist_percent1 >= 100.0) {
	// 	percent_spacing1 = "  \0";
	// } else if (moist_percent1 >= 10.0) {
	// 	percent_spacing1 = "   \0";
	// }

	// ESP_LOGI(MOIST_TAG, "moisture percent: %.4f %%%sraw: %d(dry) | %d | %d(wet)\n", moist_percent1, percent_spacing1, DRY_SENSOR_VALUE_RAW, adc_reading1, WET_SENSOR_VALUE_RAW);

	// // ESP_LOGI(MOIST_TAG, "watered: %.4f%%\traw: %d/%d\tvoltage: %dmV\n", ((double)adc_reading1 / (double)resolution) * 100, adc_reading1, resolution, voltage1);


	// ESP_LOGI(MOIST_TAG, "sensor %d:\n", 2);
	// for (int i = 0; i < NUM_SAMPLES; i++)
	// {
	// 	if (i != 0 && (i % 8) == 0)
	// 	{
	// 		printf("\n");
	// 	}

	// 	if (unit == ADC_UNIT_1)
	// 	{
	// 		adc_reading_print = adc1_get_raw((adc1_channel_t)channel2);
	// 		adc_reading2 += adc_reading_print;
	// 	}
	// 	else
	// 	{
	// 		int raw;
	// 		adc2_get_raw((adc2_channel_t)channel2, ADC_WIDTH_BIT_12, &raw);
	// 		adc_reading_print = raw;
	// 		adc_reading2 += adc_reading_print;
	// 	}

	// 	printf("%d\t", adc_reading_print);
	// }
	// printf("\n\n");

	// adc_reading2 /= NUM_SAMPLES;

	// // Convert adc_reading to voltage in mV
	// uint32_t voltage2 = esp_adc_cal_raw_to_voltage(adc_reading2, adc_chars);

	// double moist_percent2 = map((double)adc_reading2, (double)DRY_SENSOR_VALUE_RAW, (double)WET_SENSOR_VALUE_RAW, 0.0, 100.0);

	// char *percent_spacing2 = "    \0";
	
	// if (moist_percent2 >= 100.0) {
	// 	percent_spacing2 = "  \0";
	// } else if (moist_percent2 >= 10.0) {
	// 	percent_spacing2 = "   \0";
	// }

	// ESP_LOGI(MOIST_TAG, "moisture percent: %.4f %%%sraw: %d(dry) | %d | %d(wet)\n", moist_percent2, percent_spacing2, DRY_SENSOR_VALUE_RAW, adc_reading2, WET_SENSOR_VALUE_RAW);

	// // ESP_LOGI(MOIST_TAG, "watered: %.4f%%\traw: %d/%d\tvoltage: %dmV\n", ((double)adc_reading2 / (double)resolution) * 100, adc_reading2, resolution, voltage2);


	ESP_LOGI(MOIST_TAG, "sensor %d:\n", 3);
	for (int i = 0; i < NUM_SAMPLES; i++)
	{
		if (i != 0 && (i % 8) == 0)
		{
			printf("\n");
		}

		if (unit == ADC_UNIT_1)
		{
			adc_reading_print = adc1_get_raw((adc1_channel_t)channel3);
			adc_reading3 += adc_reading_print;
		}
		else
		{
			int raw;
			adc2_get_raw((adc2_channel_t)channel3, ADC_WIDTH_BIT_12, &raw);
			adc_reading_print = raw;
			adc_reading3 += adc_reading_print;
		}

		printf("%d\t", adc_reading_print);
	}
	printf("\n\n");

	adc_reading3 /= NUM_SAMPLES;

	// Convert adc_reading to voltage in mV
	uint32_t voltage3 = esp_adc_cal_raw_to_voltage(adc_reading3, adc_chars);

	double moist_percent3 = map((double)adc_reading3, (double)DRY_SENSOR_VALUE_RAW2, (double)WET_SENSOR_VALUE_RAW2, 0.0, 100.0);

	if (adc_reading3 < DRY_IF_BELOW_RAW2) {
		moist_percent3 = 0.0;
	}

	char *percent_spacing3 = "    \0";
	
	if (moist_percent3 >= 100.0) {
		percent_spacing3 = "  \0";
	} else if (moist_percent3 >= 10.0) {
		percent_spacing3 = "   \0";
	}

	ESP_LOGI(MOIST_TAG, "moisture percent: %.4f %%%sraw: %d(dry) | %d | %d(wet)\n", moist_percent3, percent_spacing3, DRY_SENSOR_VALUE_RAW2, adc_reading3, WET_SENSOR_VALUE_RAW2);

	// ESP_LOGI(MOIST_TAG, "watered: %.4f%%\traw: %d/%d\tvoltage: %dmV\n", ((double)adc_reading3 / (double)resolution) * 100, adc_reading3, resolution, voltage3);


	ESP_LOGI(MOIST_TAG, "sensor %d:\n", 4);
	for (int i = 0; i < NUM_SAMPLES; i++)
	{
		if (i != 0 && (i % 8) == 0)
		{
			printf("\n");
		}

		if (unit == ADC_UNIT_1)
		{
			adc_reading_print = adc1_get_raw((adc1_channel_t)channel4);
			adc_reading4 += adc_reading_print;
		}
		else
		{
			int raw;
			adc2_get_raw((adc2_channel_t)channel4, ADC_WIDTH_BIT_12, &raw);
			adc_reading_print = raw;
			adc_reading4 += adc_reading_print;
		}

		printf("%d\t", adc_reading_print);
	}
	printf("\n\n");

	adc_reading4 /= NUM_SAMPLES;

	// Convert adc_reading to voltage in mV
	uint32_t voltage4 = esp_adc_cal_raw_to_voltage(adc_reading4, adc_chars);

	double moist_percent4 = map((double)adc_reading4, (double)DRY_SENSOR_VALUE_RAW3, (double)WET_SENSOR_VALUE_RAW3, 0.0, 100.0);

	if (adc_reading4 < DRY_IF_BELOW_RAW3) {
		moist_percent4 = 0.0;
	}

	char *percent_spacing4 = "    \0";
	
	if (moist_percent4 >= 100.0) {
		percent_spacing4 = "  \0";
	} else if (moist_percent4 >= 10.0) {
		percent_spacing4 = "   \0";
	}

	ESP_LOGI(MOIST_TAG, "moisture percent: %.4f %%%sraw: %d(dry) | %d | %d(wet)\n", moist_percent4, percent_spacing4, DRY_SENSOR_VALUE_RAW3, adc_reading4, WET_SENSOR_VALUE_RAW3);

	// ESP_LOGI(MOIST_TAG, "watered: %.4f%%\traw: %d/%d\tvoltage: %dmV\n", ((double)adc_reading4 / (double)resolution) * 100, adc_reading4, resolution, voltage4);


	ESP_LOGI(MOIST_TAG, "sensor %d:\n", 5);
	for (int i = 0; i < NUM_SAMPLES; i++)
	{
		if (i != 0 && (i % 8) == 0)
		{
			printf("\n");
		}

		if (unit == ADC_UNIT_1)
		{
			adc_reading_print = adc1_get_raw((adc1_channel_t)channel5);
			adc_reading5 += adc_reading_print;
		}
		else
		{
			int raw;
			adc2_get_raw((adc2_channel_t)channel5, ADC_WIDTH_BIT_12, &raw);
			adc_reading_print = raw;
			adc_reading5 += adc_reading_print;
		}

		printf("%d\t", adc_reading_print);
	}
	printf("\n\n");

	adc_reading5 /= NUM_SAMPLES;

	// Convert adc_reading to voltage in mV
	uint32_t voltage5 = esp_adc_cal_raw_to_voltage(adc_reading5, adc_chars);

	double moist_percent5 = map((double)adc_reading5, (double)DRY_SENSOR_VALUE_RAW4, (double)WET_SENSOR_VALUE_RAW4, 0.0, 100.0);

	if (adc_reading5 < DRY_IF_BELOW_RAW4) {
		moist_percent5 = 0.0;
	}

	char *percent_spacing5 = "    \0";
	
	if (moist_percent5 >= 100.0) {
		percent_spacing5 = "  \0";
	} else if (moist_percent5 >= 10.0) {
		percent_spacing5 = "   \0";
	}

	ESP_LOGI(MOIST_TAG, "moisture percent: %.4f %%%sraw: %d(dry) | %d | %d(wet)\n", moist_percent5, percent_spacing5, DRY_SENSOR_VALUE_RAW4, adc_reading5, WET_SENSOR_VALUE_RAW4);

	// ESP_LOGI(MOIST_TAG, "watered: %.4f%%\traw: %d/%d\tvoltage: %dmV\n", ((double)adc_reading5 / (double)resolution) * 100, adc_reading5, resolution, voltage5);


	ESP_LOGI(MOIST_TAG, "sensor %d:\n", 6);
	for (int i = 0; i < NUM_SAMPLES; i++)
	{
		if (i != 0 && (i % 8) == 0)
		{
			printf("\n");
		}

		if (unit == ADC_UNIT_1)
		{
			adc_reading_print = adc1_get_raw((adc1_channel_t)channel6);
			adc_reading6 += adc_reading_print;
		}
		else
		{
			int raw;
			adc2_get_raw((adc2_channel_t)channel6, ADC_WIDTH_BIT_12, &raw);
			adc_reading_print = raw;
			adc_reading6 += adc_reading_print;
		}

		printf("%d\t", adc_reading_print);
	}
	printf("\n\n");

	adc_reading6 /= NUM_SAMPLES;

	// Convert adc_reading to voltage in mV
	uint32_t voltage6 = esp_adc_cal_raw_to_voltage(adc_reading6, adc_chars);

	double moist_percent6 = map((double)adc_reading6, (double)DRY_SENSOR_VALUE_RAW5, (double)WET_SENSOR_VALUE_RAW5, 0.0, 100.0);

	if (adc_reading6 < DRY_IF_BELOW_RAW5) {
		moist_percent6 = 0.0;
	}

	char *percent_spacing6 = "    \0";
	
	if (moist_percent6 >= 100.0) {
		percent_spacing6 = "  \0";
	} else if (moist_percent6 >= 10.0) {
		percent_spacing6 = "   \0";
	}

	ESP_LOGI(MOIST_TAG, "moisture percent: %.4f %%%sraw: %d(dry) | %d | %d(wet)\n", moist_percent6, percent_spacing6, DRY_SENSOR_VALUE_RAW5, adc_reading6, WET_SENSOR_VALUE_RAW5);

	// ESP_LOGI(MOIST_TAG, "watered: %.4f%%\traw: %d/%d\tvoltage: %dmV\n", ((double)adc_reading6 / (double)resolution) * 100, adc_reading6, resolution, voltage6);


	ESP_LOGI(MOIST_TAG, "sensor %d:\n", 7);
	for (int i = 0; i < NUM_SAMPLES; i++)
	{
		if (i != 0 && (i % 8) == 0)
		{
			printf("\n");
		}

		if (unit == ADC_UNIT_1)
		{
			adc_reading_print = adc1_get_raw((adc1_channel_t)channel7);
			adc_reading7 += adc_reading_print;
		}
		else
		{
			int raw;
			adc2_get_raw((adc2_channel_t)channel7, ADC_WIDTH_BIT_12, &raw);
			adc_reading_print = raw;
			adc_reading7 += adc_reading_print;
		}

		printf("%d\t", adc_reading_print);
	}
	printf("\n\n");

	adc_reading7 /= NUM_SAMPLES;

	// Convert adc_reading to voltage in mV
	uint32_t voltage7 = esp_adc_cal_raw_to_voltage(adc_reading7, adc_chars);

	double moist_percent7 = map((double)adc_reading7, (double)DRY_SENSOR_VALUE_RAW6, (double)WET_SENSOR_VALUE_RAW6, 0.0, 100.0);

	if (adc_reading7 < DRY_IF_BELOW_RAW6) {
		moist_percent7 = 0.0;
	}

	char *percent_spacing7 = "    \0";
	
	if (moist_percent7 >= 100.0) {
		percent_spacing7 = "  \0";
	} else if (moist_percent7 >= 10.0) {
		percent_spacing7 = "   \0";
	}

	ESP_LOGI(MOIST_TAG, "moisture percent: %.4f %%%sraw: %d(dry) | %d | %d(wet)\n", moist_percent7, percent_spacing7, DRY_SENSOR_VALUE_RAW6, adc_reading7, WET_SENSOR_VALUE_RAW6);

	// ESP_LOGI(MOIST_TAG, "watered: %.4f%%\traw: %d/%d\tvoltage: %dmV\n", ((double)adc_reading7 / (double)resolution) * 100, adc_reading7, resolution, voltage7);


	ESP_LOGI(MOIST_TAG, "next reading in %d seconds...\n\n", READ_MOISTURE_DELAY_MS / 1000);

	const uint32_t mutexWaitTicks = 100000;
	if (xSemaphoreTake(xMutex, (TickType_t)mutexWaitTicks) == pdTRUE)
	{
		hours_shared = hours;
		minutes_shared = minutes;
		seconds_shared = seconds;
		day_shared = day;
		month_shared = month;
		year_shared = year;

		moist_resolution_shared = resolution;

		moist_percent_shared0 = moist_percent0;
		// moist_percent_shared1 = moist_percent1;
		// moist_percent_shared2 = moist_percent2;
		moist_percent_shared3 = moist_percent3;
		moist_percent_shared4 = moist_percent4;
		moist_percent_shared5 = moist_percent5;
		moist_percent_shared6 = moist_percent6;
		moist_percent_shared7 = moist_percent7;

		moist_adc_reading_shared0 = adc_reading0;
		// moist_adc_reading_shared1 = adc_reading1;
		// moist_adc_reading_shared2 = adc_reading2;
		moist_adc_reading_shared3 = adc_reading3;
		moist_adc_reading_shared4 = adc_reading4;
		moist_adc_reading_shared5 = adc_reading5;
		moist_adc_reading_shared6 = adc_reading6;
		moist_adc_reading_shared7 = adc_reading7;
		
		moist_voltage_shared0 = voltage0;
		// moist_voltage_shared1 = voltage1;
		// moist_voltage_shared2 = voltage2;
		moist_voltage_shared3 = voltage3;
		moist_voltage_shared4 = voltage4;
		moist_voltage_shared5 = voltage5;
		moist_voltage_shared6 = voltage6;
		moist_voltage_shared7 = voltage7;
		
		xSemaphoreGive(xMutex);
	}
	else
	{
		ESP_LOGW(MOIST_TAG, "Couldn't take mutex within %d ticks. This moisture reading will not be available to view on the web page.", mutexWaitTicks);
	}

	vTaskDelay(READ_MOISTURE_DELAY_MS / portTICK_PERIOD_MS);

	return ESP_OK;
}

static void read_moisture_task(void *arg)
{
	int ret;
	struct read_moisture_arg_t *read_moisture_arg = (struct read_moisture_arg_t *)arg;

	ESP_LOGI(MOIST_TAG, "Read moisture task started\n");

	for (;;)
	{
		ret = read_moisture(read_moisture_arg);
		if (ret != ESP_OK)
		{
			break;
		}
	}

	ESP_LOGI(MOIST_TAG, "Read moisture task exiting\n");

	vTaskDelete(xTaskGetCurrentTaskHandle());
}

esp_err_t start_read_moisture(struct read_moisture_arg_t *read_moisture_arg)
{
	//Check if Two Point or Vref are burned into eFuse
	check_efuse();

	//Configure ADC
	if (unit == ADC_UNIT_1)
	{
		adc1_config_width(ADC_WIDTH_BIT_12);

		adc1_config_channel_atten(channel0, atten);
		// adc1_config_channel_atten(channel1, atten);
		// adc1_config_channel_atten(channel2, atten);
		adc1_config_channel_atten(channel3, atten);
		adc1_config_channel_atten(channel4, atten);
		adc1_config_channel_atten(channel5, atten);
		adc1_config_channel_atten(channel6, atten);
		adc1_config_channel_atten(channel7, atten);
	}
	else
	{
		adc2_config_channel_atten((adc2_channel_t)channel0, atten);
		// adc2_config_channel_atten((adc2_channel_t)channel1, atten);
		// adc2_config_channel_atten((adc2_channel_t)channel2, atten);
		adc2_config_channel_atten((adc2_channel_t)channel3, atten);
		adc2_config_channel_atten((adc2_channel_t)channel4, atten);
		adc2_config_channel_atten((adc2_channel_t)channel5, atten);
		adc2_config_channel_atten((adc2_channel_t)channel6, atten);
		adc2_config_channel_atten((adc2_channel_t)channel7, atten);
	}

	//Characterize ADC
	adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
	esp_adc_cal_value_t val_type = esp_adc_cal_characterize(unit, atten, ADC_WIDTH_BIT_12, DEFAULT_VREF, adc_chars);
	print_char_val_type(val_type);

	void *thread_routine = (void *)read_moisture_task;
	const char *const name = "read_moist";
	uint16_t stacksize = 4096;
	int prio = tskIDLE_PRIORITY + 5;
	TaskHandle_t thread = NULL;
	const BaseType_t core_id = tskNO_AFFINITY;

	int ret = xTaskCreatePinnedToCore(thread_routine, name, stacksize, read_moisture_arg, prio, &thread, core_id);
	if (ret != pdPASS)
	{
		ESP_LOGE(MOIST_TAG, "Failed starting read moisture task (pdPASS == %d)", ret);
		return ESP_FAIL;
	}

	return ESP_OK;
}
