/* HTTP Restful API Server Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "sdkconfig.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "plant_native_ota.h"
#include "sntp.h"
#include "plant_sntp.h"
#include "plant_moist.h"

void app_main(void)
{
	native_ota_main();
}
