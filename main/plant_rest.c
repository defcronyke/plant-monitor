/* HTTP Restful API Server

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include <fcntl.h>
#include "esp_http_server.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_vfs.h"
#include "cJSON.h"
#include "plant_sntp.h"
#include "plant_moist.h"

static const char *REST_TAG = "plant-httpd";

#define REST_CHECK(a, str, goto_tag, ...)                                              \
	do                                                                                 \
	{                                                                                  \
		if (!(a))                                                                      \
		{                                                                              \
			ESP_LOGE(REST_TAG, "%s(%d): " str, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
			goto goto_tag;                                                             \
		}                                                                              \
	} while (0)

#define FILE_PATH_MAX (ESP_VFS_PATH_MAX + 128)
#define SCRATCH_BUFSIZE (10240)

typedef struct rest_server_context
{
	char base_path[ESP_VFS_PATH_MAX + 1];
	char scratch[SCRATCH_BUFSIZE];
} rest_server_context_t;

#define CHECK_FILE_EXTENSION(filename, ext) (strcasecmp(&filename[strlen(filename) - strlen(ext)], ext) == 0)

/* Set HTTP response content type according to file extension */
static esp_err_t set_content_type_from_file(httpd_req_t *req, const char *filepath)
{
	const char *type = "text/plain";
	if (CHECK_FILE_EXTENSION(filepath, ".html"))
	{
		type = "text/html";
	}
	else if (CHECK_FILE_EXTENSION(filepath, ".js"))
	{
		type = "application/javascript";
	}
	else if (CHECK_FILE_EXTENSION(filepath, ".css"))
	{
		type = "text/css";
	}
	else if (CHECK_FILE_EXTENSION(filepath, ".png"))
	{
		type = "image/png";
	}
	else if (CHECK_FILE_EXTENSION(filepath, ".ico"))
	{
		type = "image/x-icon";
	}
	else if (CHECK_FILE_EXTENSION(filepath, ".svg"))
	{
		type = "text/xml";
	}
	return httpd_resp_set_type(req, type);
}

/* Send HTTP response with the contents of the requested file */
static esp_err_t rest_common_get_handler(httpd_req_t *req)
{
	rest_server_context_t *rest_context = (rest_server_context_t *)req->user_ctx;

	char filepath[FILE_PATH_MAX];

	// Find the start of any query parameters and remove them.
	uint endpos = strlen(req->uri);

	char *s = NULL;
	s = strstr(req->uri, "?");

	if (s != NULL) {
		endpos = s - req->uri;
	}

	char req_uri[endpos + 1];
	strlcpy(req_uri, req->uri, sizeof(req_uri));

	strlcpy(filepath, rest_context->base_path, sizeof(filepath));
	if (req_uri[strlen(req_uri) - 1] == '/')
	{
		strlcat(filepath, "/index.html", sizeof(filepath));
	}
	else
	{
		strlcat(filepath, req_uri, sizeof(filepath));
	}
	int fd = open(filepath, O_RDONLY, 0);
	if (fd == -1)
	{
		ESP_LOGE(REST_TAG, "Failed to open file : %s", filepath);
		/* Respond with 500 Internal Server Error */
		httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to read existing file");
		return ESP_FAIL;
	}

	set_content_type_from_file(req, filepath);

	char *chunk = rest_context->scratch;
	ssize_t read_bytes;
	do
	{
		/* Read file in chunks into the scratch buffer */
		read_bytes = read(fd, chunk, SCRATCH_BUFSIZE);
		if (read_bytes == -1)
		{
			ESP_LOGE(REST_TAG, "Failed to read file : %s", filepath);
		}
		else if (read_bytes > 0)
		{
			/* Send the buffer contents as HTTP response chunk */
			if (httpd_resp_send_chunk(req, chunk, read_bytes) != ESP_OK)
			{
				close(fd);
				ESP_LOGE(REST_TAG, "File sending failed!");
				/* Abort sending file */
				httpd_resp_sendstr_chunk(req, NULL);
				/* Respond with 500 Internal Server Error */
				httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to send file");
				return ESP_FAIL;
			}
		}
	} while (read_bytes > 0);
	/* Close file after sending complete */
	close(fd);
	ESP_LOGI(REST_TAG, "File sending complete");
	/* Respond with an empty chunk to signal HTTP response completion */
	httpd_resp_send_chunk(req, NULL, 0);
	return ESP_OK;
}

/* Simple handler for getting system handler */
static esp_err_t system_info_get_handler(httpd_req_t *req)
{
	httpd_resp_set_type(req, "application/json");
	cJSON *root = cJSON_CreateObject();
	esp_chip_info_t chip_info;
	esp_chip_info(&chip_info);
	cJSON_AddStringToObject(root, "version", IDF_VER);
	cJSON_AddNumberToObject(root, "cores", chip_info.cores);
	const char *sys_info = cJSON_Print(root);
	httpd_resp_sendstr(req, sys_info);
	free((void *)sys_info);
	cJSON_Delete(root);
	return ESP_OK;
}

/* A handler for getting moisture readings */
static esp_err_t moist_all_get_handler(httpd_req_t *req)
{
	httpd_resp_set_type(req, "application/json");

	cJSON *root = cJSON_CreateArray();

	cJSON *obj0 = cJSON_CreateObject();
	// cJSON *obj1 = cJSON_CreateObject();
	// cJSON *obj2 = cJSON_CreateObject();
	cJSON *obj3 = cJSON_CreateObject();
	cJSON *obj4 = cJSON_CreateObject();
	cJSON *obj5 = cJSON_CreateObject();
	cJSON *obj6 = cJSON_CreateObject();
	cJSON *obj7 = cJSON_CreateObject();

	const char *example_ts = "2020-05-07 10:37:59 EST\0";
	char timestamp_buf[strlen(example_ts)];

	const uint32_t mutexWaitTicks = 100000;
	if (xSemaphoreTake(xMutex, (TickType_t)mutexWaitTicks) == pdTRUE)
	{
		sprintf(timestamp_buf, "%d-%02d-%02d %02d:%02d:%02d %s", year_shared, month_shared, day_shared, hours_shared, minutes_shared, seconds_shared, MOIST_SNTP_TIMEZONE);

		cJSON_AddStringToObject(obj0, "timestamp", timestamp_buf);
		cJSON_AddNumberToObject(obj0, "raw", moist_adc_reading_shared0);
		cJSON_AddNumberToObject(obj0, "resolution", moist_resolution_shared);
		cJSON_AddNumberToObject(obj0, "voltage", moist_voltage_shared0);
		cJSON_AddNumberToObject(obj0, "watered", moist_percent_shared0);
		// cJSON_AddNumberToObject(obj0, "watered", ((float)moist_adc_reading_shared0 / (float)moist_resolution_shared) * 100.f);

		// cJSON_AddStringToObject(obj1, "timestamp", timestamp_buf);
		// cJSON_AddNumberToObject(obj1, "raw", moist_adc_reading_shared1);
		// cJSON_AddNumberToObject(obj1, "resolution", moist_resolution_shared);
		// cJSON_AddNumberToObject(obj1, "voltage", moist_voltage_shared1);
		// cJSON_AddNumberToObject(obj1, "watered", moist_percent_shared1);
		// // cJSON_AddNumberToObject(obj1, "watered", ((float)moist_adc_reading_shared1 / (float)moist_resolution_shared) * 100.f);

		// cJSON_AddStringToObject(obj2, "timestamp", timestamp_buf);
		// cJSON_AddNumberToObject(obj2, "raw", moist_adc_reading_shared2);
		// cJSON_AddNumberToObject(obj2, "resolution", moist_resolution_shared);
		// cJSON_AddNumberToObject(obj2, "voltage", moist_voltage_shared2);
		// cJSON_AddNumberToObject(obj2, "watered", moist_percent_shared2);
		// // cJSON_AddNumberToObject(obj2, "watered", ((float)moist_adc_reading_shared2 / (float)moist_resolution_shared) * 100.f);

		cJSON_AddStringToObject(obj3, "timestamp", timestamp_buf);
		cJSON_AddNumberToObject(obj3, "raw", moist_adc_reading_shared3);
		cJSON_AddNumberToObject(obj3, "resolution", moist_resolution_shared);
		cJSON_AddNumberToObject(obj3, "voltage", moist_voltage_shared3);
		cJSON_AddNumberToObject(obj3, "watered", moist_percent_shared3);
		// cJSON_AddNumberToObject(obj3, "watered", ((float)moist_adc_reading_shared3 / (float)moist_resolution_shared) * 100.f);

		cJSON_AddStringToObject(obj4, "timestamp", timestamp_buf);
		cJSON_AddNumberToObject(obj4, "raw", moist_adc_reading_shared4);
		cJSON_AddNumberToObject(obj4, "resolution", moist_resolution_shared);
		cJSON_AddNumberToObject(obj4, "voltage", moist_voltage_shared4);
		cJSON_AddNumberToObject(obj4, "watered", moist_percent_shared4);
		// cJSON_AddNumberToObject(obj4, "watered", ((float)moist_adc_reading_shared4 / (float)moist_resolution_shared) * 100.f);

		cJSON_AddStringToObject(obj5, "timestamp", timestamp_buf);
		cJSON_AddNumberToObject(obj5, "raw", moist_adc_reading_shared5);
		cJSON_AddNumberToObject(obj5, "resolution", moist_resolution_shared);
		cJSON_AddNumberToObject(obj5, "voltage", moist_voltage_shared5);
		cJSON_AddNumberToObject(obj5, "watered", moist_percent_shared5);
		// cJSON_AddNumberToObject(obj5, "watered", ((float)moist_adc_reading_shared5 / (float)moist_resolution_shared) * 100.f);

		cJSON_AddStringToObject(obj6, "timestamp", timestamp_buf);
		cJSON_AddNumberToObject(obj6, "raw", moist_adc_reading_shared6);
		cJSON_AddNumberToObject(obj6, "resolution", moist_resolution_shared);
		cJSON_AddNumberToObject(obj6, "voltage", moist_voltage_shared6);
		cJSON_AddNumberToObject(obj6, "watered", moist_percent_shared6);
		// cJSON_AddNumberToObject(obj6, "watered", ((float)moist_adc_reading_shared6 / (float)moist_resolution_shared) * 100.f);

		cJSON_AddStringToObject(obj7, "timestamp", timestamp_buf);
		cJSON_AddNumberToObject(obj7, "raw", moist_adc_reading_shared7);
		cJSON_AddNumberToObject(obj7, "resolution", moist_resolution_shared);
		cJSON_AddNumberToObject(obj7, "voltage", moist_voltage_shared7);
		cJSON_AddNumberToObject(obj7, "watered", moist_percent_shared7);
		// cJSON_AddNumberToObject(obj7, "watered", ((float)moist_adc_reading_shared7 / (float)moist_resolution_shared) * 100.f);

		cJSON_AddItemToArray(root, obj0);
		// cJSON_AddItemToArray(root, obj1);
		// cJSON_AddItemToArray(root, obj2);
		cJSON_AddItemToArray(root, obj3);
		cJSON_AddItemToArray(root, obj4);
		cJSON_AddItemToArray(root, obj5);
		cJSON_AddItemToArray(root, obj6);
		cJSON_AddItemToArray(root, obj7);

		xSemaphoreGive(xMutex);
	}
	else
	{
		ESP_LOGW(REST_TAG, "Couldn't take mutex within %d ticks. The moisture reading is currently unavailable. Please try again later.", mutexWaitTicks);
	}

	const char *sys_info = cJSON_Print(root);
	httpd_resp_sendstr(req, sys_info);
	free((void *)sys_info);
	cJSON_Delete(root);
	return ESP_OK;
}

static esp_err_t util_activate_post_handler(httpd_req_t *req) {
	httpd_resp_set_type(req, "application/json");

	cJSON *root = cJSON_CreateObject();

	cJSON_AddStringToObject(root, "msg", "success");
	cJSON_AddNumberToObject(root, "statusCode", 200);

	const char *sys_info = cJSON_Print(root);
	httpd_resp_sendstr(req, sys_info);

	free((void *)sys_info);
	cJSON_Delete(root);

	esp_restart();

	return ESP_OK;
}

esp_err_t start_rest_server(const char *base_path)
{
	REST_CHECK(base_path, "wrong base path", err);
	rest_server_context_t *rest_context = calloc(1, sizeof(rest_server_context_t));
	REST_CHECK(rest_context, "No memory for rest context", err);
	strlcpy(rest_context->base_path, base_path, sizeof(rest_context->base_path));

	httpd_handle_t server = NULL;
	httpd_config_t config = HTTPD_DEFAULT_CONFIG();
	config.uri_match_fn = httpd_uri_match_wildcard;

	ESP_LOGI(REST_TAG, "Starting HTTP Server");
	REST_CHECK(httpd_start(&server, &config) == ESP_OK, "Start server failed", err_start);

	/* URI handler for fetching system info */
	httpd_uri_t system_info_get_uri = {
		.uri = "/api/v1/system/info",
		.method = HTTP_GET,
		.handler = system_info_get_handler,
		.user_ctx = rest_context};
	httpd_register_uri_handler(server, &system_info_get_uri);

	/* URI handler for moisture sensor readings */
	httpd_uri_t moist_all_get_uri = {
		.uri = "/api/v1/moist/all",
		.method = HTTP_GET,
		.handler = moist_all_get_handler,
		.user_ctx = rest_context};
	httpd_register_uri_handler(server, &moist_all_get_uri);

	/* URI handler for moisture sensor readings */
	httpd_uri_t util_activate_post_uri = {
		.uri = "/api/v1/util/activate",
		.method = HTTP_POST,
		.handler = util_activate_post_handler,
		.user_ctx = rest_context};
	httpd_register_uri_handler(server, &util_activate_post_uri);

	/* URI handler for getting web server files */
	httpd_uri_t common_get_uri = {
		.uri = "/*",
		.method = HTTP_GET,
		.handler = rest_common_get_handler,
		.user_ctx = rest_context};
	httpd_register_uri_handler(server, &common_get_uri);

	return ESP_OK;
err_start:
	free(rest_context);
err:
	return ESP_FAIL;
}
