#ifndef __MOIST_H_INCLUDED__
#define __MOIST_H_INCLUDED__

#include <stdint.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>

#define READ_MOISTURE_DELAY_MS 15000 // Take a moisture reading this often.

struct read_moisture_arg_t
{

};

extern SemaphoreHandle_t xMutex;

extern int hours_shared, minutes_shared, seconds_shared, day_shared, month_shared, year_shared;

extern uint32_t moist_resolution_shared;

extern double moist_percent_shared0;
// extern double moist_percent_shared1;
// extern double moist_percent_shared2;
extern double moist_percent_shared3;
extern double moist_percent_shared4;
extern double moist_percent_shared5;
extern double moist_percent_shared6;
extern double moist_percent_shared7;

extern uint32_t moist_adc_reading_shared0;
// extern uint32_t moist_adc_reading_shared1;
// extern uint32_t moist_adc_reading_shared2;
extern uint32_t moist_adc_reading_shared3;
extern uint32_t moist_adc_reading_shared4;
extern uint32_t moist_adc_reading_shared5;
extern uint32_t moist_adc_reading_shared6;
extern uint32_t moist_adc_reading_shared7;

extern uint32_t moist_voltage_shared0;
// extern uint32_t moist_voltage_shared1;
// extern uint32_t moist_voltage_shared2;
extern uint32_t moist_voltage_shared3;
extern uint32_t moist_voltage_shared4;
extern uint32_t moist_voltage_shared5;
extern uint32_t moist_voltage_shared6;
extern uint32_t moist_voltage_shared7;

#endif