#!/bin/sh

mkdir -p server_certs

cd server_certs

openssl dhparam -out dhparam.pem 4096

cd ..
