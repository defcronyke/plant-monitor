#!/bin/sh

mkdir -p server_certs

cd server_certs

openssl req -x509 -newkey rsa:4096 -keyout ca_key.pem -out ca_cert.pem -days 365000 -nodes

cd ..
