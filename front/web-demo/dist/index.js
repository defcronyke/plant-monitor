(function() {
    // console.log('Index.');

    var cameraButton = document.getElementById('camera');

    cameraButton.addEventListener('click', function(e) {
        e.preventDefault();

        // console.log('browsing to camera');

        window.open('http://eye1.eternalvoid.net', '_blank');
    });

    var timeout_ms = 15000;

    function nextReadings(timeout_ms) {
        console.log('Next moisture readings in ' + (timeout_ms / 1000) + ' seconds...');

        window.setTimeout(function() {
            getMoisture(timeout_ms);
        }, timeout_ms);
    }

    function scalePercentToRange(percent, round, invert, rangeMin, rangeMax, percentMin, percentMax) {
        if (typeof percent === 'undefined') {
            console.log('error: you must call scalePercentToRange with a percentage as the first argument');
            return null;
        }

        var rMin = rangeMin;
        var rMax = rangeMax;
        
        if (typeof rangeMin === 'undefined') {
            rMin = 0;
        }

        if (typeof rangeMax === 'undefined') {
            rMax = 255;
        }

        var pMin = percentMin;
        var pMax = percentMax;

        if (typeof pMin === 'undefined') {
            pMin = 1;
        }

        if (typeof pMax === 'undefined') {
            pMax = 100;
        }

        var p = Math.min(Math.max(percent, pMin), pMax);

        // console.log('p: ', p);

        var res = null;

        if (invert) {
            // console.log('inverted');
            res = ((((rMax - rMin) * (pMax - p - pMin)) / (pMax - pMin)) + rMin);
        } else {
            // console.log('not inverted');
            res = (((rMax - rMin) * (p - pMin)) / (pMax - pMin)) + rMin;
        }

        if (round) {
            res = Math.round(res);
        }

        // console.log('percent scaled to range: ', res);
        
        return res;
    }

    function getMoisture(timeout_ms) {
        console.log('Fetching moisture readings...');

        var options = {};

        fetch('/api/v1/moist/all', options)
        .then(
            function(res) {
                return res.json();
            },
            function(err) {
                console.log('error fetching moisture readings (json promise rejected):');
                console.log(err);

                return err;
            }
        )
        .then(
            function(res) {
                if (!res.length || (res.length > 0 && (!res[0].watered && res[0].watered !== 0))) {
                    console.log('error fetching moisture readings (data not received):');
                    console.log(res);

                    nextReadings(timeout_ms);

                    return res;
                }

                console.log('moisture readings:');
                console.log(res);

                var moistTable = document.getElementById('moist');

                var newMoistTable = document.createElement('table');
                newMoistTable.setAttribute('id', 'moist');
                newMoistTable.setAttribute('border', '1');

                moistTable.replaceWith(newMoistTable);

                var h_tr = document.createElement('tr');
                newMoistTable.appendChild(h_tr);

                var h_td1 = document.createElement('td');
                h_tr.appendChild(h_td1);
                var h_span1 = document.createElement('span');
                h_span1.innerText = res[0].timestamp;
                h_td1.appendChild(h_span1);

                var h_td2 = document.createElement('td');
                h_tr.appendChild(h_td2);
                var h_span2 = document.createElement('span');
                h_span2.innerText = 'watered';
                h_td2.appendChild(h_span2);

                for (var i = 0; i < res.length; i++) {
                    var sensor = res[i];

                    var tr = document.createElement('tr');
                    newMoistTable.appendChild(tr);

                    var td = document.createElement('td');
                    tr.appendChild(td);
                    var span = document.createElement('span');
                    span.innerText = 'Plant ' + (i + 1);
                    td.appendChild(span);

                    var watered_td = document.createElement('td');
                    watered_td.setAttribute('id', 'watered_td' + (i + 1));
                    watered_td.setAttribute('class', 'watered_td');
                    tr.appendChild(watered_td);
                    var watered_span = document.createElement('span');
                    watered_span.innerText = parseFloat(sensor.watered.toFixed(6)) + '%';
                    watered_span.setAttribute('id', 'watered_span' + (i + 1));
                    watered_span.setAttribute('class', 'watered_span');
                    watered_td.appendChild(watered_span);
                    
                    watered_td.appendChild(document.createElement('br'));

                    // Watered visual percentage bar.
                    var watered_prog_span = document.createElement('span');
                    watered_prog_span.innerHTML = '&nbsp';
                    watered_prog_span.setAttribute('id', 'watered_prog_span' + (i + 1));
                    watered_prog_span.setAttribute('class', 'watered_prog_span');
                    watered_prog_span.style.width = Math.max(sensor.watered, 5) + '%';
                    
                    // Change background colour depending on percentage.
                    watered_prog_span.style.backgroundColor = 'rgba(' + 
                        scalePercentToRange(sensor.watered, true, true) + 
                        ', 69, ' + 
                        scalePercentToRange(sensor.watered, true, false, 0, 235) + 
                        ', 1.0)';

                    watered_td.appendChild(watered_prog_span);
                }

                nextReadings(timeout_ms);

                return res;
            },
            function(err) {
                console.log('error fetching moisture readings (promise rejected):');
                console.log(err);

                nextReadings(timeout_ms);
                
                return err;
            }
        )
        .catch(function(err) {
            console.log('error fetching moisture readings (promise caught):');
            console.log(err);

            nextReadings(timeout_ms);

            return err;
        });
    }

    getMoisture(timeout_ms);
})();
