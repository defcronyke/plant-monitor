(function(){
    console.log('Util.');

    var activateButton = document.getElementById('activate');

    activateButton.addEventListener('click', function(e) {
        e.preventDefault();

        console.log('activating');

        fetch('/api/v1/util/activate', {
            method: 'POST'
        })
        .then(
            function(res) {
                console.log('activate response:');
                console.log(res);
            },
            function(err) {
                console.log('activate error (promise rejected):');
                console.log(err);
            }
        )
        .catch(function(err) {
            console.log('activate error (promise caught):');
            console.log(err);
        });
    });
})();
